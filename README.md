# Videos
This is the UI to this [api.](https://gitlab.com/avosa/avosa_20221012/-/tree/main)

The frontend has been hosted on Vercel and is live [here](https://video-upload-gray.vercel.app/).

The backend/api has been hosted on heroku and is live [here](https://api-my-hosp.herokuapp.com/).

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

Enjoy!

# Author

[Webster Avosa](https://github.com/avosa)
