import Vue from 'vue'
import VueMeta from 'vue-meta'
import Axios from './plugins/axios'
import App from './App.vue'
import router from './router'

// components
import Navbar from '@/components/common/Navbar.vue';

// styles
import 'bootstrap/dist/js/bootstrap.bundle.min'
import "@/assets/scss/styles.scss";
import store from './store'

Vue.component('Navbar', Navbar);
Vue.config.productionTip = false

Vue.use(Axios);
Vue.use(VueMeta)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
