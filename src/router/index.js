import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store';
import { getRedirectQueries } from '@/functions';
import Dashboard from '../pages/dashboard/Dashboard'

Vue.use(VueRouter);

function loadView(view) {
  return () =>
    import(/* webpackChunkName: "view-[request]" */ `@/pages/${view}`);
}

const routes = [
  {
    path: '/',
    component: Dashboard,
    children: [
      {
        path: '',
        name: 'dashboard',
        component: loadView('dashboard/DashboardHome.vue'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'videos/upload',
        name: 'videos-upload',
        component: loadView('dashboard/UploadVideo.vue'),
        meta: {
          requiresAuth: true
        }
      },
    ],
  },
  {
    path: '/auth/login',
    name: 'login',
    component: loadView('auth/Login.vue')
  },
  {
    path: '/auth/signup',
    name: 'signup',
    component: loadView('auth/Signup.vue')
  }
]

function scrollBehavior(to, from, savedPosition) {
  if (savedPosition) {
    // savedPosition is only available for popstate navigations.
    return savedPosition
  }
  return new Promise((resolve) => {
    // wait for the out transition to complete (if necessary)
    setTimeout(() => {
      // if the returned position is falsy or an empty object,
      // will retain current scroll position.
      resolve({ x: 0, y: 0 })
    }, 250)
  })
}

const router = new VueRouter({
  mode: 'history',
  routes,
  scrollBehavior,
});

router.beforeEach((to, _, next) => {
  if (!store.getters.isAuthenticated && to.meta.requiresAuth)
    next({ name: 'login', query: getRedirectQueries(to) });
  else next();
});

export default router
