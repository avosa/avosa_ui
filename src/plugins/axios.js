

import Vue from 'vue';
import axios from "axios";
import store from '@/store';
import router from '@/router';
import { getRedirectQueries } from '@/functions';

// Full config:  https://github.com/axios/axios#request-config
axios.defaults.headers.common.Authorization = store.getters.token;
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

const config = {
  baseURL: process.env.VUE_APP_API_BASE_URL || "http://localhost:3000"
  // timeout: 60 * 1000, // Timeout
  // withCredentials: true, // Check cross-site Access-Control
};

const localAxios = axios.create(config);

localAxios.interceptors.request.use(
  (conf) =>
    // Do something before request is sent
    conf
  ,
  (error) =>
    // Do something with request error
    Promise.reject(error)

);

// Add a response interceptor
localAxios.interceptors.response.use(
  (response) =>
    // Do something with response data
    response
  ,
  (error) => {
    if (error) {
      const originalRequest = error.config;

      if (error.response.status === 401 && !originalRequest._retry) { // eslint-disable-line no-underscore-dangle

        originalRequest._retry = true; // eslint-disable-line no-underscore-dangle
        store.commit('logOut')
        return router.push({ name: 'login', query: getRedirectQueries(router.currentRoute) })
      }
    }
    return Promise.reject(error)
  }

);

;

export default {
  install: (app) => {
    app.axios = localAxios;
    window.axios = localAxios;
    Object.defineProperties(Vue.prototype, {
      axios: {
        get() {
          return localAxios;
        }
      },
      $axios: {
        get() {
          return localAxios;
        }
      },
    });
  }
};
