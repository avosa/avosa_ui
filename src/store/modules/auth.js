/* eslint-disable no-param-reassign */

import { getFormData } from "@/functions";

export default {
    state: {
        user: JSON.parse(localStorage.getItem('user')),
        token: JSON.parse(localStorage.getItem('token')),
    },
    getters: {
        isAuthenticated: state => state.user && state.token,
        user: state => state.user,
        token: state => state.token,
    },
    mutations: {
        setUser(state, user) {
            state.user = user
        },
        setUserToken(state, token) {
            state.token = token
        },
        logOut(state) {
            state.user = null
            state.token = null
            localStorage.setItem('user', JSON.stringify(state.user))
            localStorage.setItem('token', JSON.stringify(state.token))
        },
    },
    actions: {
        async register({ dispatch }, signupData) {
            const signupFormData = getFormData(signupData);
            const { status } = await window.axios.post("/users", signupFormData);
            if (status === 201) {
                await dispatch('login', { email: signupData.email, password: signupData.password })
            }
        },
        async login({ commit }, { email, password }) {
            const { data } = await window.axios.post("/users/login", {
                email,
                password
            });
            commit('setUser', data.user)
            commit('setUserToken', data.token)
            localStorage.setItem('user', JSON.stringify(data.user))
            localStorage.setItem('token', JSON.stringify(data.token))
        }
    },
}