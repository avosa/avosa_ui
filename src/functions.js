export const getFormData = signupData => {
    const formData = new FormData();
    Object.keys(signupData).forEach((key) => {
        formData.append(key, signupData[key]);
    });
    return formData;
}

export const getRedirectQueries = to => {
    const query = {
        redirect: to.name,
    };

    Object.getOwnPropertyNames(to.params).forEach((key) => {
        query[key] = to.params[key];
    });

    return query;
}

export const getNextRoute = (ownRouteQuery, alternative) => {
    if (ownRouteQuery.redirect)
        return {
            name: ownRouteQuery.redirect,
            params: { ...ownRouteQuery },
        };
    return { name: alternative || 'dashboard' };
};